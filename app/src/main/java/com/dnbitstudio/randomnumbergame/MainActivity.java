package com.dnbitstudio.randomnumbergame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private int randomInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(100) + 1;

        // Toast.makeText(this, "Our random number is " + randomInt, Toast.LENGTH_SHORT).show();
        Log.d("My app", "Our random number is " + randomInt);

        editText = (EditText) findViewById(R.id.edit_text);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkNumber();
            }
        });
    }

    public void checkNumber() {
        // Toast.makeText(MainActivity.this, "Button clicked", Toast.LENGTH_SHORT).show();
        int editTextValue = Integer.valueOf((editText.getText().toString()));
        // "45" -> 45
        // "hello" -> ???

        if (editTextValue == randomInt) {
            Toast.makeText(this, "WELL DONE!", Toast.LENGTH_SHORT).show();
        } else if (editTextValue > randomInt) {
            Toast.makeText(this, "Too High", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Too Low", Toast.LENGTH_SHORT).show();
        }
    }
}
